﻿using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.BLL
{
    class QBRCalculator
    {
        public SeasonStats Calculator(SeasonStats seasonStats)
        {
            decimal weightedCompletionPercentage = ((seasonStats.Completions / seasonStats.Attempts * 100) - 30) * .05m;

            decimal weightedYardsPerAttempt = ((seasonStats.Yards / seasonStats.Attempts) - 3) * .25m;

            decimal weightedPercentageOfTouchdownPasses = (seasonStats.Touchdowns / seasonStats.Attempts * 100)* .2m;

            decimal weightedPercentageOfInterceptions = 2.375m - (seasonStats.Interceptions / seasonStats.Attempts * 100*.25m);

            seasonStats.QBR = Math.Round((weightedCompletionPercentage + weightedYardsPerAttempt + weightedPercentageOfTouchdownPasses + weightedPercentageOfInterceptions) / 6 * 100,1);

            return seasonStats;
        }
    }
}
