﻿using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.BLL
{
    public class QuarterbackStatsManager
    {
        private IPlayerRepository playerRepository;
        private ISeasonStatsRepository seasonStatsRepository;

        public QuarterbackStatsManager(IPlayerRepository playerRepository, ISeasonStatsRepository seasonStatsRepository)
        {
            this.playerRepository = playerRepository;
            this.seasonStatsRepository = seasonStatsRepository;
        }

        public List<Player> GetAllPlayers()
        {
            try
            {
                return playerRepository.GetAllPlayers();
            }
            catch
            {
                return null;
            }
        }

        public Player GetPlayerByLastName(string lastName)
        {
            if(string.IsNullOrEmpty(lastName))
            {
                return null;
            }
            try
            {
                Player player = playerRepository.GetPlayerByLastName(lastName);
                if (player == null)
                {
                    return null;
                }
                return player;
            }
            catch
            {
                return null;
            }

        }
        public Player AddPlayer(Player player)
        {
            if (string.IsNullOrEmpty(player.FirstName))
            {
                return null;
            }
            if (string.IsNullOrEmpty(player.LastName))
            {
                return null;
            }
            try
            {
                return playerRepository.AddPlayer(player);
            }
            catch
            {
                return null;
            }
        }

        public bool EditPlayer(Player player)
        {
            if (string.IsNullOrEmpty(player.FirstName))
            {
                return false;
            }
            if (string.IsNullOrEmpty(player.LastName))
            {
                return false;
            }
            try
            {
                var success = playerRepository.EditPlayerInfo(player);
                if (success)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool RemovePlayer(int playerID)
        {
            if(playerID<0)
            {
                return false;
            }
            try
            {
                var success = playerRepository.RemovePlayer(playerID);
                if (success)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public List<SeasonStats> GetCareerStats(int playerID)
        {
            if(playerID<1)
            {
                return null;
            }
            List<SeasonStats> stats = seasonStatsRepository.GetCareerStats(playerID);
            if(stats==null)
            {
                return null;
            }
            return stats;
        }

        public SeasonStats GetSeasonStats(int playerID, int year)
        {
            if(playerID<1 || year<1920)
            {
                return null;
            }
            SeasonStats seasonStats = seasonStatsRepository.GetSeasonStats(playerID, year);
            if(seasonStats==null)
            {
                return null;
            }
            return seasonStats;
        }

        public SeasonStats AddSeasonStats(SeasonStats seasonStats)
        { 
            if(seasonStats.Attempts<1 || seasonStats.Attempts<(seasonStats.Completions+seasonStats.Interceptions) || seasonStats.Attempts>999)
            {
                return null;
            }
            if(seasonStats.Touchdowns>seasonStats.Completions ||seasonStats.Touchdowns<0 || seasonStats.Touchdowns>99)
            {
                return null;
            }
            if(seasonStats.Interceptions<0 || seasonStats.Interceptions>99)
            {
                return null;
            }
            if(seasonStats.Completions<0 || seasonStats.Completions>999)
            {
                return null;
            }
            if(seasonStats.Yards>8000)
            {
                return null;
            }

            QBRCalculator qbrCalculator = new QBRCalculator();
            seasonStats.QBR = qbrCalculator.Calculator(seasonStats).QBR;
            return seasonStatsRepository.AddSeasonStats(seasonStats);
        }

        public bool EditSeasonStats(SeasonStats seasonStats)
        {
            if(seasonStats.Attempts<1 || seasonStats.Attempts<(seasonStats.Completions+seasonStats.Interceptions) || seasonStats.Attempts>999)
            {
                return false;
            }
            if(seasonStats.Touchdowns>seasonStats.Completions ||seasonStats.Touchdowns<0 || seasonStats.Touchdowns>99)
            {
                return false;
            }
            if(seasonStats.Interceptions<0 || seasonStats.Interceptions>99)
            {
                return false;
            }
            if(seasonStats.Completions<0 || seasonStats.Completions>999)
            {
                return false;
            }
            if(seasonStats.Yards>8000)
            {
                return false;
            }
            QBRCalculator qbrCalculator = new QBRCalculator();
            seasonStats.QBR = qbrCalculator.Calculator(seasonStats).QBR;
            var success = seasonStatsRepository.EditSeasonStats(seasonStats);
            if (success)
            {
                return true;
            }
            return false;
        }

        public bool RemoveCareerStats(int playerID)
        {
            if(playerID < 1)
            {
                return false;
            }
            var success = seasonStatsRepository.RemoveCareerStats(playerID);
            if (success)
            {
                return true;
            }
            return false;
        }

        public bool RemoveSeasonStats(int playerID, int year)
        {
            if(playerID<1 || year<1920 || year>DateTime.Now.Year)
            {
                return false;
            }
            var success = seasonStatsRepository.RemoveSingleSeasonStats(playerID, year);
            if (success)
            {
                return true;
            }
            return false;
        }
    }
}
