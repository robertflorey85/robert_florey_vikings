﻿using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.BLL.Tests.MockSeasonStatsRepositories
{
    public class WorkingMockSeasonStatsRepository : ISeasonStatsRepository
    {
        private List<SeasonStats> _seasonStats = new List<SeasonStats>()
        {
            new SeasonStats
            {
                PlayerID=1,
                Year=1994,
                Completions=324,
                Touchdowns=35,
                Interceptions=10,
                Attempts=461,
                Yards=3969,
                QBR=112.8m
            }
        };

        public SeasonStats AddSeasonStats(SeasonStats seasonStats)
        {
            _seasonStats.Add(seasonStats);
            return seasonStats;
        }

        public bool EditSeasonStats(SeasonStats seasonStats)
        {
            SeasonStats currentSeasonStats = _seasonStats.FirstOrDefault(s => s.Year == seasonStats.Year && s.PlayerID == seasonStats.PlayerID);
            if(currentSeasonStats==null)
            {
                return false;
            }
            _seasonStats.Remove(currentSeasonStats);
            _seasonStats.Add(seasonStats);
            return true;
        }

        public List<SeasonStats> GetCareerStats(int playerID)
        {
            List<SeasonStats> careerStats = new List<SeasonStats>();
            foreach(SeasonStats seasonStats in _seasonStats)
            {
                if (seasonStats.PlayerID == playerID)
                {
                    careerStats.Add(seasonStats);
                }
            }
            return careerStats;
        }

        public SeasonStats GetSeasonStats(int playerID, int year)
        {
            SeasonStats seasonStats = new SeasonStats();
            foreach(SeasonStats item in _seasonStats)
            {
                if(item.PlayerID==playerID && item.Year==year)
                {
                    return item;
                }
            }
            return null;
        }

        public bool RemoveCareerStats(int playerID)
        {
            List<SeasonStats> currentCareerStats = new List<SeasonStats>();
            foreach(SeasonStats seasonStats in _seasonStats)
            {
                if(seasonStats.PlayerID==playerID)
                {
                    currentCareerStats.Add(seasonStats);
                    _seasonStats.Remove(seasonStats);
                }
            }
            if (currentCareerStats.Count==0)
            {
                return false;
            }
            return true;
        }

        public bool RemoveSingleSeasonStats(int playerID, int year)
        {
            SeasonStats currentSeasonStats = _seasonStats.FirstOrDefault(s => s.Year == year && s.PlayerID == playerID);
            if (currentSeasonStats == null)
            {
                return false;
            }
            _seasonStats.Remove(currentSeasonStats);
            return true;
        }
    }
}
