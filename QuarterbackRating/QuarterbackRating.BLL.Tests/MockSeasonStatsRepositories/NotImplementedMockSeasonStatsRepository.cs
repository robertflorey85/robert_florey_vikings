﻿using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.BLL.Tests.MockSeasonStatsRepositories
{
    public class NotImplementedMockSeasonStatsRepository : ISeasonStatsRepository
    {
        public SeasonStats AddSeasonStats(SeasonStats seasonStats)
        {
            throw new NotImplementedException();
        }

        public bool EditSeasonStats(SeasonStats seasonStats)
        {
            throw new NotImplementedException();
        }

        public List<SeasonStats> GetCareerStats(int playerID)
        {
            throw new NotImplementedException();
        }

        public SeasonStats GetSeasonStats(int playerID, int year)
        {
            throw new NotImplementedException();
        }

        public bool RemoveCareerStats(int playerID)
        {
            throw new NotImplementedException();
        }

        public bool RemoveSingleSeasonStats(int playerID, int year)
        {
            throw new NotImplementedException();
        }
    }
}
