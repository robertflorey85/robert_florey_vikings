﻿using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.BLL.Tests.MockSeasonStatsRepositories
{
    public class FalseNullMockSeasonStatsRepository : ISeasonStatsRepository
    {
        public SeasonStats AddSeasonStats(SeasonStats seasonStats)
        {
            return null;
        }

        public bool EditSeasonStats(SeasonStats seasonStats)
        {
            return false;
        }

        public List<SeasonStats> GetCareerStats(int playerID)
        {
            return null;
        }

        public SeasonStats GetSeasonStats(int playerID, int year)
        {
            return null;
        }

        public bool RemoveCareerStats(int playerID)
        {
            return false;
        }

        public bool RemoveSingleSeasonStats(int playerID, int year)
        {
            return false;
        }
    }
}
