﻿using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.BLL.Tests.MockPlayerRepositories
{
    public class FalseNullMockPlayerRepository : IPlayerRepository
    {
        public Player AddPlayer(Player player)
        {
            return null;
        }

        public bool EditPlayerInfo(Player player)
        {
            return false;
        }

        public List<Player> GetAllPlayers()
        {
            return null;
        }

        public Player GetPlayerByLastName(string lastName)
        {
            return null;
        }

        public bool RemovePlayer(int playerID)
        {
            return false;
        }
    }
}
