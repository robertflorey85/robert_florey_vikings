﻿using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.BLL.Tests.MockPlayerRepositories
{
    public class WorkingMockPlayerRepository : IPlayerRepository
    {
        private List<Player> _players = new List<Player>()
        {
            new Player
            {
                FirstName= "Steve",
                LastName= "Young",
                PlayerID= 1
            },
            new Player
            {
                FirstName= "Joe",
                LastName= "Montana",
                PlayerID= 2
            }
        };
        public Player AddPlayer(Player player)
        {
            _players.Add(player);
            return player;
        }

        public bool EditPlayerInfo(Player player)
        {
            Player currentPlayerInfo = _players.FirstOrDefault(p => p.PlayerID == player.PlayerID);
            if(currentPlayerInfo==null)
            {
                return false;
            }
            _players.Remove(currentPlayerInfo);
            _players.Add(player);
            return true;
        }

        public List<Player> GetAllPlayers()
        {
            return _players;
        }

        public Player GetPlayerByLastName(string lastName)
        {
            foreach(Player player in _players)
            {
                if( player.LastName==lastName)
                {
                    return player;
                }
            }
            return null;
        }

        public bool RemovePlayer(int playerID)
        {
            Player player = new Player();
            player = _players.FirstOrDefault(p => p.PlayerID == playerID);
            if(player==null)
            {
                return false;
            }
            int index = _players.IndexOf(player);
            _players.RemoveAt(index);
            return true;
            
        }
    }
}
