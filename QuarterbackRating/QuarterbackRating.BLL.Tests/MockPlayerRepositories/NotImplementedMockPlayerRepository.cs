﻿using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.BLL.Tests.MockPlayerRepositories
{
    public class NotImplementedMockPlayerRepository : IPlayerRepository
    {
        public Player AddPlayer(Player player)
        {
            throw new NotImplementedException();
        }

        public bool EditPlayerInfo(Player player)
        {
            throw new NotImplementedException();
        }

        public List<Player> GetAllPlayers()
        {
            throw new NotImplementedException();
        }

        public Player GetPlayerByLastName(string lastName)
        {
            throw new NotImplementedException();
        }

        public bool RemovePlayer(int playerID)
        {
            throw new NotImplementedException();
        }
    }
}
