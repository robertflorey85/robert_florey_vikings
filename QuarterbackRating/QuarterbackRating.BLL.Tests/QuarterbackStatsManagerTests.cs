﻿using NUnit.Framework;
using QuarterbackRating.BLL;
using QuarterbackRating.BLL.Tests.MockPlayerRepositories;
using QuarterbackRating.BLL.Tests.MockSeasonStatsRepositories;
using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.BLL.Tests
{
    [TestFixture]
    public class QuarterbackStatsManagerTests
    {
        private QuarterbackStatsManager manager;

        [Test]
        public void CanGetPlayerByLastName()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            string lastName = "Montana";
            Player player = manager.GetPlayerByLastName(lastName);
            Assert.IsNotNull(player);
        }

        [Test]
        public void CanNotGetPlayerLastNameDoesNotExist()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            string lastName = "Brady";
            Player player = manager.GetPlayerByLastName(lastName);
            Assert.IsNull(player);
        }

        [Test]
        public void CanNotGetPlayerExceptionThrown()
        {
            var playerRepository = new NotImplementedMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            string lastName = "Montana";
            Player player = manager.GetPlayerByLastName(lastName);
            Assert.IsNull(player);
        }

        [Test]
        public void CanAddPlayer()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            Player player = new Player()
            {
                FirstName= "Rich",
                LastName= "Gannon",
                PlayerID=3
            };
            Player playerAdded = manager.AddPlayer(player);
            Assert.IsNotNull(playerAdded);
        }

        [Test]
        public void CanNotAddPlayerWithoutFirstName()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            Player player = new Player()
            {
                LastName = "Gannon",
                PlayerID = 3
            };
            Player playerAdded = manager.AddPlayer(player);
            Assert.IsNull(playerAdded);
        }

        [Test]
        public void CanNotAddPlayerWithoutLastName()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            Player player = new Player()
            {
                FirstName = "Rich",
                PlayerID = 3
            };
            Player playerAdded = manager.AddPlayer(player);
            Assert.IsNull(playerAdded);
        }

        [Test]
        public void CanNotAddPlayerExceptionThrown()
        {
            var playerRepository = new NotImplementedMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            Player player = new Player()
            {
                FirstName = "Rich",
                LastName = "Gannon",
                PlayerID = 3
            };
            Player playerAdded = manager.AddPlayer(player);
            Assert.IsNull(playerAdded);
        }

        [Test]
        public void CanEditPlayer()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            Player player = new Player()
            {
                FirstName = "Rich",
                LastName = "Gannon",
                PlayerID = 2
            };
            var success = manager.EditPlayer(player);
            Assert.IsTrue(success);
        }

        [Test]
        public void CanNotEditPlayerIDDoesNotExist()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            Player player = new Player()
            {
                FirstName = "Rich",
                LastName = "Gannon",
                PlayerID = 9999
            };
            var success = manager.EditPlayer(player);
            Assert.IsFalse(success);
        }

        [Test]
        public void CanNotEditPlayerToHaveNoFirstName()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            Player player = new Player()
            {
                LastName = "Gannon",
                PlayerID = 2
            };
            var success = manager.EditPlayer(player);
            Assert.IsFalse(success);
        }

        [Test]
        public void CanNotEditPlayerToHaveNoLastName()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            Player player = new Player()
            {
                FirstName = "Rich",
                PlayerID = 2
            };
            var success = manager.EditPlayer(player);
            Assert.IsFalse(success);
        }

        [Test]
        public void CanNotEditPlayerExceptionThrown()
        {
            var playerRepository = new NotImplementedMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            Player player = new Player()
            {
                FirstName = "Rich",
                LastName = "Gannon",
                PlayerID = 2
            };
            var success = manager.EditPlayer(player);
            Assert.IsFalse(success);
        }

        [Test]
        public void CanRemovePlayer()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            var success = manager.RemovePlayer(1);
            Assert.IsTrue(success);
        }

        [Test]
        public void CanNotRemoveNegativeID()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            var success = manager.RemovePlayer(-2);
            Assert.IsFalse(success);
        }

        [Test]
        public void CanNotRemovePlayerIDDoesNotExist()
        {
            var playerRepository = new WorkingMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            var success = manager.RemovePlayer(9999);
            Assert.IsFalse(success);
        }

        [Test]
        public void CanNotRemoveExceptionThrown()
        {
            var playerRepository = new NotImplementedMockPlayerRepository();
            var seasonStatsRepository = new WorkingMockSeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            var success = manager.RemovePlayer(1);
            Assert.IsFalse(success);
        }
    }
}
