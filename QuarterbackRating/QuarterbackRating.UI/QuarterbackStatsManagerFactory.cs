﻿using QuarterbackRating.BLL;
using QuarterbackRating.Data.FileRepositories;
using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.UI
{
    public static class QuarterbackStatsManagerFactory
    {
        public static QuarterbackStatsManager GetQuarterbackStatsManager()
        {
            QuarterbackStatsManager manager;

            IPlayerRepository playerRepository = new PlayerRepository();
            ISeasonStatsRepository seasonStatsRepository = new SeasonStatsRepository();
            manager = new QuarterbackStatsManager(playerRepository, seasonStatsRepository);
            return manager;
        }
    }
}
