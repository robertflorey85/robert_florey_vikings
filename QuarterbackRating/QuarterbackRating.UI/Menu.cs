﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.UI
{
    class Menu
    {
        public Menu()
        {
            Console.WriteLine("Please pick a number from the options below (default is to quit.)");
            Console.WriteLine("1. List all Players");
            Console.WriteLine("2. Display player's single season QBR");
            Console.WriteLine("3. List player's career stats by season");
            Console.WriteLine("4. List player's single season stats");
            Console.WriteLine("5. Add new player");
            Console.WriteLine("6. Add new season stats to existing player");
            Console.WriteLine("7. Update player info");
            Console.WriteLine("8. Update player stats");
            Console.WriteLine("9. Remove player");
            Console.WriteLine("10. Remove single season stats");
            Console.WriteLine("11. Quit");
        }

        public int GetUserMenuPick()
        {
            int userPick;
            int.TryParse(Console.ReadLine(), out userPick);
            return userPick;
        }
    }
}
