﻿using QuarterbackRating.BLL;
using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.UI
{
    class UIController
    {
        QuarterbackStatsManager manager = QuarterbackStatsManagerFactory.GetQuarterbackStatsManager();

        public UIController()
        {
            bool quit = false;
            while (!quit)
            {
                Menu menu = new Menu();
                UserPrompts prompt = new UserPrompts();
                int userPick = menu.GetUserMenuPick();
                Console.Clear();
                switch(userPick)
                {
                    case 1:
                        {
                            var players = manager.GetAllPlayers().OrderBy(p => p.LastName);
                            foreach(Player player in players)
                            {
                                Console.WriteLine("{0} {1}", player.FirstName, player.LastName);
                            }
                            Console.WriteLine("Press enter to return to main menu.");
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 2:
                        {
                            string lastName = prompt.GetLastName();
                            Player player = manager.GetPlayerByLastName(lastName);
                            if (player == null)
                            {
                                Console.WriteLine("Could not find player with that last name. Press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            int year = prompt.GetYearOfSeason(player);
                            SeasonStats seasonStats = manager.GetSeasonStats(player.PlayerID, year);
                            if (seasonStats==null)
                            {
                                Console.WriteLine("Could not find player stats for that year. Press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.WriteLine("{0} {1} had a QB Rating of {2} in {3}", player.FirstName, player.LastName, seasonStats.QBR, seasonStats.Year);
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 3:
                        {
                            string lastName = prompt.GetLastName();
                            Player player = manager.GetPlayerByLastName(lastName);
                            if (player == null)
                            {
                                Console.WriteLine("Could not find player with that last name. Press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            var careerStats = manager.GetCareerStats(player.PlayerID).OrderBy(s => s.Year);
                            if (careerStats == null)
                            {
                                Console.WriteLine("Could not find player stats. Press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.Clear();
                            foreach(SeasonStats seasonStats in careerStats)
                            {
                                Console.WriteLine("{0} {1}'s stats in {2} were:", player.FirstName, player.LastName, seasonStats.Year);
                                Console.WriteLine("Completions: {0}", seasonStats.Completions);
                                Console.WriteLine("Attempts: {0}", seasonStats.Attempts);
                                Console.WriteLine("Passing Yards: {0}", seasonStats.Yards);
                                Console.WriteLine("Touchdowns: {0}", seasonStats.Touchdowns);
                                Console.WriteLine("Interceptions: {0}", seasonStats.Interceptions);
                                Console.WriteLine("Quarterback Rating: {0}", seasonStats.QBR);
                                Console.WriteLine("");
                            }
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 4:
                        {
                            string lastName = prompt.GetLastName();
                            Player player = manager.GetPlayerByLastName(lastName);
                            if (player == null)
                            {
                                Console.WriteLine("Could not find player with that last name. Press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            int year = prompt.GetYearOfSeason(player);
                            SeasonStats seasonStats = manager.GetSeasonStats(player.PlayerID, year);
                            if (seasonStats == null)
                            {
                                Console.WriteLine("Could not find player stats for that year. Press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.WriteLine("{0} {1}'s stats in {2} were:", player.FirstName, player.LastName, seasonStats.Year);
                            Console.WriteLine("Completions: {0}", seasonStats.Completions);
                            Console.WriteLine("Attempts: {0}", seasonStats.Attempts);
                            Console.WriteLine("Passing Yards: {0}", seasonStats.Yards);
                            Console.WriteLine("Touchdowns: {0}", seasonStats.Touchdowns);
                            Console.WriteLine("Interceptions: {0}", seasonStats.Interceptions);
                            Console.WriteLine("Quarterback Rating: {0}", seasonStats.QBR);
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 5:
                        {
                            string firstName = prompt.GetFirstName();
                            string lastName = prompt.GetLastName();
                            Console.WriteLine("{0} {1}.  Is this correct?(y/n)", firstName, lastName);
                            if (Console.ReadLine().ToLower() == "y")
                            {
                                Player player = new Player()
                                {
                                    FirstName = firstName,
                                    LastName = lastName
                                };
                                var addedPlayer = manager.AddPlayer(player);
                                if(addedPlayer != null)
                                {
                                    Console.WriteLine("Player added successfully. Press enter to return to menu.");
                                }
                                else
                                {
                                    Console.WriteLine("Player add failed. Press enter to return to main menu.");
                                }
                            }
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 6:
                        {
                            string lastName = prompt.GetLastName();
                            SeasonStats seasonStats = new SeasonStats();
                            Player player = manager.GetPlayerByLastName(lastName);
                            if (player == null)
                            {
                                Console.WriteLine("Failed to find player, press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }

                            seasonStats.PlayerID = player.PlayerID;
                            seasonStats.Year = prompt.GetYearOfSeason(player);
                            seasonStats.Completions = prompt.GetPassCompletions(player, seasonStats);
                            seasonStats.Touchdowns = prompt.GetTouchdownsThrown(player, seasonStats);
                            seasonStats.Interceptions = prompt.GetInterceptionsThrown(player, seasonStats);
                            seasonStats.Attempts = prompt.GetPassAttempts(player, seasonStats);
                            seasonStats.Yards = prompt.GetPassingYards(player, seasonStats);
                            Console.Clear();
                            Console.WriteLine("Year: {0}, Completions: {1}, Touchdowns: {2}, Interceptions: {3}, Attempts: {4}, Yards:{5}", seasonStats.Year, seasonStats.Completions, seasonStats.Touchdowns, seasonStats.Interceptions, seasonStats.Attempts, seasonStats.Yards);
                            Console.WriteLine("Does this look correct press 'y' to add, otherwise return to main menu.");
                            if( Console.ReadLine().ToLower()!="y")
                            {
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }

                            if (manager.AddSeasonStats(seasonStats) == null)
                            {
                                Console.WriteLine("Failed to add season stats, press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.WriteLine("Added season stats successfully, press enter to return to main menu.");
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 7:
                        {
                            string lastName = prompt.GetLastName();
                            Player player = manager.GetPlayerByLastName(lastName);
                            if (player==null)
                            {
                                Console.WriteLine("Failed to find player, press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.WriteLine("Would you like to correct his first name? (press y then enter to update)");
                            if(Console.ReadLine().ToLower()=="y")
                            {
                                player.FirstName = prompt.GetFirstName();
                            }
                            Console.WriteLine("Would you like to edit his last name? (press y then enter to update)");
                            if (Console.ReadLine().ToLower()=="y")
                            {
                                player.LastName = prompt.GetLastName();
                            }
                            var successEditing = manager.EditPlayer(player);
                            if(successEditing)
                            {
                                Console.WriteLine("Player info successfully updated. Press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.WriteLine("Failed to update player, press enter to return to main menu.");
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 8:
                        {
                            string lastName = prompt.GetLastName();
                            Player player = manager.GetPlayerByLastName(lastName);
                            if (player == null)
                            {
                                Console.WriteLine("Failed to find player, press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            int year = prompt.GetYearOfSeason(player);
                            SeasonStats seasonStats = manager.GetSeasonStats(player.PlayerID, year);
                            if (seasonStats == null)
                            {
                                Console.WriteLine("Could not find player stats for that year. Press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.WriteLine("Would you like to edit his completions? (press y then enter to update)");
                            if (Console.ReadLine().ToLower() == "y")
                            {
                                seasonStats.Completions=prompt.GetPassCompletions(player,seasonStats);
                            }
                            Console.WriteLine("Would you like to edit his touchdowns thrown? (press y then enter to update)");
                            if (Console.ReadLine().ToLower() == "y")
                            {
                                seasonStats.Touchdowns= prompt.GetTouchdownsThrown(player, seasonStats);
                            }
                            Console.WriteLine("Would you like to edit his interceptions thrown? (press y then enter to update)");
                            if (Console.ReadLine().ToLower() == "y")
                            {
                                seasonStats.Interceptions = prompt.GetInterceptionsThrown(player, seasonStats);
                            }
                            Console.WriteLine("Would you like to edit his pass attempts? (press y then enter to update)");
                            if (Console.ReadLine().ToLower() == "y")
                            {
                                seasonStats.Attempts = prompt.GetPassAttempts(player, seasonStats);
                            }
                            Console.WriteLine("Would you like to edit his pass yards? (press y then enter to update)");
                            if (Console.ReadLine().ToLower() == "y")
                            {
                                seasonStats.Yards = prompt.GetPassingYards(player, seasonStats);
                            }
                            var success = manager.EditSeasonStats(seasonStats);
                            if (success)
                            {
                                Console.WriteLine("Season's stats successfully updated. Press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.WriteLine("Failed to update season's stats, press enter to return to main menu.");
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }

                    case 9:
                        {
                            string lastName = prompt.GetLastName();
                            Player player = manager.GetPlayerByLastName(lastName);
                            if (player == null)
                            {
                                Console.WriteLine("Failed to find player, press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.WriteLine("Are you sure you want to remove {0} {1} and all his stats? Press y and enter to remove.", player.FirstName, player.LastName);
                            if(Console.ReadLine().ToLower()=="y")
                            {
                                var deleteStatsSuccess = manager.RemoveCareerStats(player.PlayerID);
                                if(deleteStatsSuccess)
                                {
                                    var deleteSuccess = manager.RemovePlayer(player.PlayerID);
                                    if(deleteSuccess)
                                    {
                                        Console.WriteLine("Player and stats successfully removed. Press enter to return to main menu.");
                                        Console.ReadKey();
                                        Console.Clear();
                                        break;
                                    }
                                    Console.WriteLine("Deleted stats but failed to delete player. Press enter to return to main menu.");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                                }
                                var deletePlayerSuccess = manager.RemovePlayer(player.PlayerID);
                                if (deletePlayerSuccess)
                                {
                                    Console.WriteLine("Successfully deleted player. Press enter to return to main menu.");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;
                                }
                                Console.WriteLine("Failed to delete stats and player. Press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.Clear();
                            break;
                        }

                    case 10:
                        {
                            string lastName = prompt.GetLastName();
                            Player player = manager.GetPlayerByLastName(lastName);
                            if (player == null)
                            {
                                Console.WriteLine("Failed to find player, press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            int year = prompt.GetYearOfSeason(player);
                            SeasonStats seasonStats = manager.GetSeasonStats(player.PlayerID, year);
                            if (seasonStats == null)
                            {
                                Console.WriteLine("Could not find player stats for that year. Press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.WriteLine("{0} {1}'s stats in {2} were:", player.FirstName, player.LastName, seasonStats.Year);
                            Console.WriteLine("Completions: {0}", seasonStats.Completions);
                            Console.WriteLine("Attempts: {0}", seasonStats.Attempts);
                            Console.WriteLine("Passing Yards: {0}", seasonStats.Yards);
                            Console.WriteLine("Touchdowns: {0}", seasonStats.Touchdowns);
                            Console.WriteLine("Interceptions: {0}", seasonStats.Interceptions);
                            Console.WriteLine("Quarterback Rating: {0}", seasonStats.QBR);
                            Console.WriteLine("Are you sure you want to delete these stats? Press 'y' then enter to delete");
                            if (Console.ReadLine().ToLower() == "y")
                            {
                                var success = manager.RemoveSeasonStats(player.PlayerID, seasonStats.Year);
                                if (success)
                                {
                                    Console.Clear();
                                    Console.WriteLine("Season successfully removed. Press enter to return to main menu.");
                                    Console.ReadKey();
                                    Console.Clear();
                                    break;

                                }
                                Console.WriteLine("Failed to remove season, press enter to return to main menu.");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }
                            Console.Clear();
                            break;
                        }

                    case 11:
                    default:
                        {
                            Console.WriteLine("Are you sure you want to quit? y/n");
                            if (Console.ReadLine().ToLower() == "y")
                            {
                                quit = true;
                            }
                            Console.Clear();
                            break;
                        }

                }

            }
        }
    }
}
