﻿using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.UI
{
    class UserPrompts
    {
        public string GetFirstName()
        {
            Console.WriteLine("Please enter player's first name.");
            string firstName = Console.ReadLine();
            while(firstName.Length<2)
            {
                Console.WriteLine("Please enter a name with at least two letters.");
                firstName = Console.ReadLine();
            }
            return firstName;
        }

        public string GetLastName()
        {
            Console.WriteLine("Please enter player's last name.");
            string lastName = Console.ReadLine();
            while (lastName.Length < 2)
            {
                Console.WriteLine("Please enter a name with at least two letters.");
                lastName = Console.ReadLine();
            }
            return lastName;
        }

        public int GetYearOfSeason(Player player)
        {
            Console.WriteLine("Please enter the year of the season for {0} {1}", player.FirstName, player.LastName);
            string possibleYear = Console.ReadLine();
            int year;
            int.TryParse(possibleYear, out year);
            int maxYear = DateTime.Now.Year;
            while(year<1920 || year > maxYear)
            {
                Console.WriteLine("Please enter a valid four digit year.");
                possibleYear = Console.ReadLine();
                int.TryParse(possibleYear, out year);
            }
            return year;
        }

        public decimal GetPassCompletions(Player player, SeasonStats seasonStats)
        {
            Console.WriteLine("Please enter the number of completions thrown by {0} {1} in {2}.", player.FirstName, player.LastName, seasonStats.Year);
            string completionsString = Console.ReadLine();
            decimal completions;
            decimal.TryParse(completionsString, out completions);
            while (completions<0 || completions > 999)
            {
                Console.WriteLine("Please enter a valid number of completions between 0 and 1000.");
                completionsString = Console.ReadLine();
                decimal.TryParse(completionsString, out completions);
            }
            return completions;
        }

        public decimal GetTouchdownsThrown(Player player, SeasonStats seasonStats)
        {
            Console.WriteLine("Please enter the number of touchdowns thrown by {0} {1} in {2}.", player.FirstName, player.LastName, seasonStats.Year);
            string touchdownsString = Console.ReadLine();
            decimal touchdowns;
            decimal.TryParse(touchdownsString, out touchdowns);
            while (touchdowns < 0 || touchdowns > 99 || touchdowns > seasonStats.Completions)
            {
                Console.WriteLine("Please enter a valid number of touchdowns between 0 and 100.");
                touchdownsString = Console.ReadLine();
                decimal.TryParse(touchdownsString, out touchdowns);
            }
            return touchdowns;
        }

        public decimal GetInterceptionsThrown(Player player, SeasonStats seasonStats)
        {
            Console.WriteLine("Please enter the number of interceptions thrown by {0} {1} in {2}.", player.FirstName, player.LastName, seasonStats.Year);
            string interceptionsString = Console.ReadLine();
            decimal interceptions;
            decimal.TryParse(interceptionsString, out interceptions);
            while (interceptions < 0 || interceptions > 99)
            {
                Console.WriteLine("Please enter a valid number of interceptions between 0 and 100.");
                interceptionsString = Console.ReadLine();
                decimal.TryParse(interceptionsString, out interceptions);
            }
            return interceptions;
        }

        public decimal GetPassAttempts(Player player, SeasonStats seasonStats)
        {
            Console.WriteLine("Please enter the number of attempts thrown by {0} {1} in {2}.", player.FirstName, player.LastName, seasonStats.Year);
            string attemptsString = Console.ReadLine();
            decimal attempts;
            decimal.TryParse(attemptsString, out attempts);
            while (attempts < 1 || attempts > 999 || attempts<seasonStats.Completions+seasonStats.Interceptions)
            {
                Console.WriteLine("Please enter a valid number of attempts between 0 and 1000.");
                attemptsString = Console.ReadLine();
                decimal.TryParse(attemptsString, out attempts);
            }
            return attempts;
        }

        public decimal GetPassingYards(Player player, SeasonStats seasonStats)
        {
            Console.WriteLine("Please enter the number of pass yards thrown by {0} {1} in {2}.", player.FirstName, player.LastName, seasonStats.Year);
            string yardsString = Console.ReadLine();
            decimal passYards;
            decimal.TryParse(yardsString, out passYards);
            while (passYards < 0 || passYards > 8000)
            {
                Console.WriteLine("Please enter a valid number of pas yards between 0 and 8000.");
                yardsString = Console.ReadLine();
                decimal.TryParse(yardsString, out passYards);
            }
            return passYards;
        }
    }
}
