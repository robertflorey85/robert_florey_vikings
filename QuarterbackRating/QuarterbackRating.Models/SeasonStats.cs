﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.Models
{
    public class SeasonStats
    {
        public int PlayerID { get; set; }
        public int Year { get; set; }
        public decimal Attempts { get; set; }
        public decimal Completions { get; set; }
        public decimal Yards { get; set; }
        public decimal Touchdowns { get; set; }
        public decimal Interceptions { get; set; }
        public decimal QBR { get; set; }
    }
}
