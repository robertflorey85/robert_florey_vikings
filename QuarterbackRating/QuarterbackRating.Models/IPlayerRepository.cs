﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.Models
{
    public interface IPlayerRepository
    {
        List<Player> GetAllPlayers();
        Player GetPlayerByLastName(string lastName);
        Player AddPlayer(Player player);
        bool EditPlayerInfo(Player player);
        bool RemovePlayer(int playerID);
    }
}
