﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.Models
{
    public interface ISeasonStatsRepository
    {
        List<SeasonStats> GetCareerStats(int playerID);
        SeasonStats GetSeasonStats(int playerID, int year);
        SeasonStats AddSeasonStats(SeasonStats seasonStats);
        bool EditSeasonStats(SeasonStats seasonStats);
        bool RemoveSingleSeasonStats(int playerID, int year);
        bool RemoveCareerStats(int playerID);
    }
}
