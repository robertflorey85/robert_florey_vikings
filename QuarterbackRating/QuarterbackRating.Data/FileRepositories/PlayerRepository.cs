﻿using Newtonsoft.Json;
using QuarterbackRating.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace QuarterbackRating.Data.FileRepositories
{
    public class PlayerRepository : IPlayerRepository
    {
        public Player AddPlayer(Player player)
        {
            var players = GetAllPlayers();
            if (players.Count == 0)
            {
                player.PlayerID = 1;
            }
            else
            {
                player.PlayerID = players.Max(p => p.PlayerID) + 1;
            }
            string path = string.Format(@"..\Information\Players\{0}.json", player.PlayerID);
            var serializer = new JsonSerializer();
            using (var stream = new StreamWriter(path))
            {
                using (JsonWriter writer = new JsonTextWriter(stream))
                {
                    serializer.Serialize(writer, player);
                }
            }
            return player;
        }

        public bool EditPlayerInfo(Player player)
        {
            string path = string.Format(@"..\Information\Players\{0}.json", player.PlayerID);
            var serializer = new JsonSerializer();
            using (var stream = new StreamWriter(path))
            {
                using (JsonWriter writer = new JsonTextWriter(stream))
                {
                    serializer.Serialize(writer, player);
                }
            }
            return true;
        }

        public List<Player> GetAllPlayers()
        {
            string path = @"..\Information\Players";
            Player player;
            List<Player> players = new List<Player>();
            foreach(string file in Directory.EnumerateFiles(path))
            {
                using (StreamReader stream = new StreamReader(file))
                {
                    string json = stream.ReadToEnd();
                    player = JsonConvert.DeserializeObject<Player>(json);
                }
                players.Add(player);
            }
            return players;
        }

        public Player GetPlayerByLastName(string lastName)
        {
            string path = @"..\Information\Players";
            Player player;
            foreach (string file in Directory.EnumerateFiles(path))
            {
                using (StreamReader stream = new StreamReader(file))
                {
                    string json = stream.ReadToEnd();
                    player = JsonConvert.DeserializeObject<Player>(json);
                }
                if (player.LastName.ToLower()==lastName.ToLower())
                {
                    return player;
                }
            }
            return null;
        }

        public bool RemovePlayer(int playerID)
        {
            string path = string.Format(@"..\Information\Players\{0}.json", playerID);
            if (!File.Exists(path))
            {
                return false;
            }
            File.Delete(path);
            return true;
        }
       
    }
}
