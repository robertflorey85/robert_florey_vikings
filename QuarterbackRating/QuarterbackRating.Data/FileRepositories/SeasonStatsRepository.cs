﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuarterbackRating.Models;
using System.IO;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;

namespace QuarterbackRating.Data.FileRepositories
{
    public class SeasonStatsRepository : ISeasonStatsRepository
    {
        public SeasonStats AddSeasonStats(SeasonStats seasonStats)
        {
            List<SeasonStats> stats;
            string path = string.Format(@"..\Information\SeasonStats\{0}.json", seasonStats.PlayerID);
            var serializer = new JsonSerializer();
            if (!File.Exists(path))
            {
                using (var stream = new StreamWriter(path))
                {
                    using (JsonWriter writer = new JsonTextWriter(stream))
                    {
                        stats = new List<SeasonStats>();
                        stats.Add(seasonStats);
                        serializer.Serialize(writer, stats);
                    }
                }
            }
            else
            {
                
                using (StreamReader stream = new StreamReader(path))
                {
                    string json = stream.ReadToEnd();
                    stats = JsonConvert.DeserializeObject<List<SeasonStats>>(json);                    
                }
                if (stats.Exists(s => s.Year == seasonStats.Year))
                {
                    return null;
                }
                stats.Add(seasonStats);
                string newJson = JsonConvert.SerializeObject(stats);
                File.WriteAllText(path, newJson);
            }
            return seasonStats;
        }

        public bool EditSeasonStats(SeasonStats seasonStats)
        {
            List<SeasonStats> stats;
            string path = string.Format(@"..\Information\SeasonStats\{0}.json", seasonStats.PlayerID);
            if(!File.Exists(path))
            {
                return false;
            }
            var serializer = new JsonSerializer();

            using (StreamReader stream = new StreamReader(path))
            {
                string json = stream.ReadToEnd();
                stats = JsonConvert.DeserializeObject<List<SeasonStats>>(json);
            }
            if (stats.FirstOrDefault(s => s.Year == seasonStats.Year) == null)
            {
                return false;
            }
            int indexToEdit = stats.FindIndex(s => s.Year == seasonStats.Year);
            stats.RemoveAt(indexToEdit);
            stats.Add(seasonStats);
            string newJson = JsonConvert.SerializeObject(stats);
            File.WriteAllText(path, newJson);
            return true;
        }

        public List<SeasonStats> GetCareerStats(int playerID)
        {
            string path = string.Format(@"..\Information\SeasonStats\{0}.json", playerID);
            if(!File.Exists(path))
            {
                return null;
            }
            List<SeasonStats> stats;
            using (StreamReader stream = new StreamReader(path))
            {
                string json = stream.ReadToEnd();
                stats = JsonConvert.DeserializeObject<List<SeasonStats>>(json);
            }
            return stats;
        }

        public SeasonStats GetSeasonStats(int playerID, int year)
        {
            string path = string.Format(@"..\Information\SeasonStats\{0}.json",playerID);
            List<SeasonStats> stats;
                using (StreamReader stream = new StreamReader(path))
            {
                string json = stream.ReadToEnd();
                stats = JsonConvert.DeserializeObject<List<SeasonStats>>(json);
            }
            foreach(SeasonStats seasonStats in stats)
            {
                if (seasonStats.Year == year)
                {
                    return seasonStats;
                }

            }
            
            return null;

        }

        public bool RemoveCareerStats(int playerID)
        {
            string path = string.Format(@"..\Information\SeasonStats\{0}.json", playerID);
            if (!File.Exists(path))
            {
                return false;
            }
            File.Delete(path);
            return true;
        }

        public bool RemoveSingleSeasonStats(int playerID, int year)
        {
            List<SeasonStats> stats;
            string path = string.Format(@"..\Information\SeasonStats\{0}.json", playerID);
            if(!File.Exists(path))
            {
                return false;
            }
            var serializer = new JsonSerializer();

            using (StreamReader stream = new StreamReader(path))
            {
                string json = stream.ReadToEnd();
                stats = JsonConvert.DeserializeObject<List<SeasonStats>>(json);
            }
            if (stats.FirstOrDefault(s => s.Year == year)==null)
            {
                return false;
            }
            if(stats.Count==1)
            {
                File.Delete(path);
                return true;
            }
            int indexToEdit = stats.FindIndex(s => s.Year == year);
            stats.RemoveAt(indexToEdit);
            string newJson = JsonConvert.SerializeObject(stats);
            File.WriteAllText(path, newJson);
            return true;
        }
    }
}
